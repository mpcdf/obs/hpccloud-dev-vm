terraform {
  required_version = ">= 1.0.0"
  required_providers {
    ansible = {
      source  = "ansible/ansible"
      version = "~> 1.1.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.5.1"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6.0"
    }
  }
}

provider "openstack" {
  auth_url = "https://hpccloud.mpcdf.mpg.de:13000"
}
