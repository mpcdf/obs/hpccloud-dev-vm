locals {
  instances = tomap({
    raven = tomap({
      image = "openSUSE Leap 15.5"
      os_family = "Suse"
    })
    viper = tomap({
      image = "CentOS Stream 9"
      os_family = "RedHat"
    })
  })
}

data "openstack_images_image_v2" "image" {
  for_each = local.instances
  name     = each.value.image
}

data "openstack_compute_flavor_v2" "mpcdf-flavor" {
  name = "mpcdf.large"
}

data "openstack_networking_network_v2" "cloud-local-obs" {
  name = "cloud-local-obs"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "random_uuid" "kp" {}

resource "openstack_compute_keypair_v2" "kp" {
  name = "obs-${random_uuid.kp.id}"
}

resource "local_sensitive_file" "ssh-key" {
  content              = openstack_compute_keypair_v2.kp.private_key
  filename             = "${abspath(path.module)}/.ssh/id_rsa"
  directory_permission = "0700"
  file_permission      = "0600"
}

resource "random_uuid" "vol" {}

resource "openstack_blockstorage_volume_v3" "vol" {
  for_each = local.instances
  name = "obs-${each.key}-${random_uuid.vol.id}"
  size = 200
}

locals {
  user_data = templatefile("${path.module}/user_data.tftpl", {
    public_key = openstack_compute_keypair_v2.kp.public_key
  })
}

resource "openstack_compute_instance_v2" "vm" {
  for_each        = local.instances
  name            = "obs-dev-${each.key}"
  image_id        = data.openstack_images_image_v2.image[each.key].id
  flavor_id       = data.openstack_compute_flavor_v2.mpcdf-flavor.id
  key_pair        = openstack_compute_keypair_v2.kp.name
  security_groups = [data.openstack_networking_secgroup_v2.default.name]
  user_data       = local.user_data

  network {
    name = data.openstack_networking_network_v2.cloud-local-obs.name
  }

  block_device {
    uuid                  = data.openstack_images_image_v2.image[each.key].id
    source_type           = "image"
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    uuid                  = openstack_blockstorage_volume_v3.vol[each.key].id
    source_type           = "volume"
    destination_type      = "volume"
    boot_index            = 1
    delete_on_termination = true
  }
}

output "instance_ip_address" {
  value = {
    for k, v in local.instances: k => openstack_compute_instance_v2.vm[k].access_ip_v4
  }
}

resource "ansible_host" "vm" {
  for_each = local.instances
  name = openstack_compute_instance_v2.vm[each.key].name
  variables = {
    ansible_host                 = openstack_compute_instance_v2.vm[each.key].access_ip_v4
    ansible_user                 = "ansible"
    ansible_os_family            = each.value.os_family
    ansible_ssh_private_key_file = local_sensitive_file.ssh-key.filename
  }
}
