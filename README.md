# Prerequisites

1. Install *Ansible* and *Terraform* first. Ansible can be installed using pip:
   ```console
   $ pip3 install ansible
   ```
   Terraform can be downloaded [here](https://developer.hashicorp.com/terraform/install) as a single binary.

2. Then, install additional Ansible modules:
   ```console
   $ cd ansible
   $ ansible-galaxy install -r requirements.yml
   ```

3. You need an application credential to talk to the OpenStack API.
   To create a new application credential go here:
   https://hpccloud.mpcdf.mpg.de/dashboard/identity/application_credentials/

   The *ID* can be exported as `OS_APPLICATION_CREDENTIAL_ID` and the *Secret*
   can be exported as `OS_APPLICATION_CREDENTIAL_SECRET`.

# Start the VM

Start the VM with:

```console
$ cd terraform
$ terraform init
$ terraform apply
```

Enter `yes` and wait a bit until the host becomes available.
Terraform will print the IP address of the newly created instance.
You can print it again with `terraform output`.

Wait a few seconds and then apply the Ansible configuration:

```console
$ cd ansible
$ ansible-playbook main.yml
```

Confirm your username and accept the host fingerprint with `yes`.
Login with your Kerberos user and password via SSH using the IP address printed before.
You can do that automatically with:
```console
$ cd terraform
$ ssh "$(terraform output -raw instance_ip_address)"
```

After login, run the bootstrap script:

```console
<user>@obs-dev-vm:~> bootstrap.sh
```

# Stop and destroy the VM

```console
$ cd terraform
$ terraform destroy
```

Enter `yes` and wait a bit.
